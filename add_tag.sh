#!/bin/bash
#This tag tags hosts with whatever tag ID is provided under TagSimple. The list of agent IDs is added to assets.txt

while read p
do
curl --location --request POST 'https://qualysapi.qg3.apps.qualys.com/qps/rest/2.0/update/am/hostasset' \
--header 'Content-Type: text/xml' \
--header 'X-Requested-With: QualysPostman' \
--header 'Authorization: Basic AUTH' \
--data-raw '<ServiceRequest>
   <filters>
       <Criteria field="agentUuid" operator="EQUALS">'$p'</Criteria>
   </filters>
   <data>
       <HostAsset>
          <tags>
               <add>
                   <TagSimple>
                       <id>TAG ID FROM QUALYS</id>
                   </TagSimple>
               </add>
           </tags>
       </HostAsset>
   </data>
</ServiceRequest>'
done < assets.txt
